<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AlerUserOfInsecureAccess extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $dataAccess;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $dataAccess)
    {
        $this->user = $user;
        $this->dataAccess = $dataAccess;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.alert-user-of-insecure-access')
            ->subject('Hemos detectado que has iniciado sesión desde una lugar que no consideraste como seguro');
    }
}
