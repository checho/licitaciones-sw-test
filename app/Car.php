<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    protected $fillable = [
        'car_brand',
        'car_model',
        'car_plate',
        'car_seats',
        'user_id',
    ];

    // Mutators

        // Setters

        // Getters

    // Relarionships

    public function User()
    {
       return $this->belongsTo(User::class);
    }

    // Methods
}
