<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserAccess extends Model
{
    protected $fillable = [
        'regexp',
        'languages',
        'browser',
        'browser_version',
        'platform',
        'platform_version',
        'device',
        'device_type',
        'my_ip',
        'user_id',
    ];

    // Mutators

        // Setters

        public function setLanguagesAttribute($languages)
        {            
            $this->attributes['languages'] = implode(', ', $languages);            
        }

        // Getters

    // Relarionships

    public function User()
    {
        return $this->belongsTo(User::class);
    }

    // Methods

    
}
