<?php

namespace App\Listeners;

use Jenssegers\Agent\Agent;
use Illuminate\Support\Facades\Session;
use App\Events\userHasBeenAuthenticated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Notifications\AlertUserOfInsecureAccess;

class checkIpAccess
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Handle the event.
     *
     * @param  userHasBeenAuthenticated  $event->request, $event->user
     * @return void
     */
    public function handle(userHasBeenAuthenticated $event)
    {                
        $REMOTE_ADDR = $event->request->ip();

        if (count($event->user->Accesses) > 0) {
            foreach ($event->user->Accesses as $access) {        
                if ($REMOTE_ADDR != $access->my_ip) {                    
                    Session::flash('warning', 'Hemos detectado que estas navegando desde un lugar que no consideraste como seguro');
                    $event->user->notify(new AlertUserOfInsecureAccess($this->dataAccessInformation()));
                    return redirect()->back();
                } else {                    
                    Session::flash('info', 'Navegas de forma segura :)');
                    return redirect()->back();
                }
            }
        }                
    }

    public function dataAccessInformation()
    {
        $agent = new Agent();

        $data['regexp'] = $agent->match('regexp');
        $data['languages'] = $agent->languages();

        $data['browser'] = $agent->browser();
        $data['browser_version'] = $agent->version($data['browser']);

        $data['platform'] = $agent->platform();
        $data['platform_version'] = $agent->version($data['platform']);

        $data['device'] = $agent->device();

        if ($agent->isDesktop()) {
            $data['device_type'] = "Desktop";
        }

        if ($agent->isPhone()) {
            if ($agent->isMobile()) {
                $data['device_type'] = "Mobile";
            }
            if ($agent->isTablet()) {
                $data['device_type'] = "Tablet";
            }

            if ($agent->isAndroidOS()) {
                $data['device_os'] = "Android";
            }
            if ($agent->isNexus()) {
                $data['device_os'] = "Nexus";
            }
            if ($agent->isSafari()) {
                $data['device_os'] = "Safari";
            }
            if ($agent->is('iPhone')) {
                $data['device_os'] = "iPhone";
            }
        }

        $data['my_ip'] = $_SERVER['REMOTE_ADDR'];
        
        return $data;
    }
}
