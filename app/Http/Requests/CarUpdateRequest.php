<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class CarUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'car_brand' => 'required|string',
            'car_model' => 'required|string',
            'car_plate' =>  [
                'required', 
                'alpha_dash', 
                'min:5', 
                'max:8', 
                Rule::unique('cars')->ignore($this->route('car')->id)
                ],
            'car_seats' => 'required|numeric|integer|min:1|max:50',

            'user_id' => 'required|numeric|integer',            
        ];        
    }
}
