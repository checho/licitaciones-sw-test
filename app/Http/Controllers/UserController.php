<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Events\UserWasCreated;
use App\Http\Requests\UserRequest;
use App\Http\Requests\UserUpdateRequest;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return view('admin.users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {        

        $password = str_random(8);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => $password
        ]);

        UserWasCreated::dispatch($user, $password);

        return redirect()->route('users.index')->with('info', 'Se ha creado un usuario exitosamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return view('admin.users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        // return view('admin.users.edit', compact('user'));

        return $user;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(UserUpdateRequest $request, User $user)
    {
        $user->update($request->validated());

        return redirect()->route('users.index')->with('info', 'Se ha actualizado el usuario exitosamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        // if($user->id === auth()->user()->id){            

        //     return redirect()->back()->with('error', 'No te puedes eliminar a ti mismo');
        // } 
        
        // $user->delete(); 
                    
        // return redirect()->back()->with('info', 'Se ha eliminado el usuario exitosamente');                  

        if($user->id === auth()->user()->id){
            $response = [
                'error'   => 'No te puedes eliminar a ti mismo',                
            ];

            return $response;
        } else {
            $user->Cars()->delete();
            $user->delete();

            $response = [
                'success'   => 'Se ha eliminado el usuario exitosamente',
                'codigo'  => '200',
            ];
            
            return $response;
        }                
    }
}
