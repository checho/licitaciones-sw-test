<?php

namespace App\Http\Controllers;

use Auth;
use App\UserAccess;
use Jenssegers\Agent\Agent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class SaveDataAccessController extends Controller
{
    public function saveDataAccess(Request $request)
    {        
        $agent = new Agent();        

        $data['regexp'] = $agent->match('regexp');
        $data['languages'] = $agent->languages();

        $data['browser'] = $agent->browser();
        $data['browser_version'] = $agent->version($data['browser']);

        $data['platform'] = $agent->platform();
        $data['platform_version'] = $agent->version($data['platform']);

        $data['device'] = $agent->device();
        
        if ($agent->isDesktop()) {
            $data['device_type'] = "Desktop";
        }
        
        if ($agent->isPhone()) {
            if ($agent->isMobile()) {
                $data['device_type'] = "Mobile";
            }
            if ($agent->isTablet()) {
                $data['device_type'] = "Tablet";
            }

            if ($agent->isAndroidOS()) {
                $data['device_os'] = "Android";
            }
            if ($agent->isNexus()) {
                $data['device_os'] = "Nexus";
            }
            if ($agent->isSafari()) {
                $data['device_os'] = "Safari";
            }
            if ($agent->is('iPhone')) {
                $data['device_os'] = "iPhone";
            }
        }
        
        $data['my_ip'] = $_SERVER['REMOTE_ADDR'];
        $data['user_id'] = Auth::user()->id;
        
        $userAccess = UserAccess::create($data);

        Session::flash('info', 'Navegas de forma segura :)');

        return redirect()->back();
    }
}
