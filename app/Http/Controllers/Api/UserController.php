<?php

namespace App\Http\Controllers\Api;

use App\User;
use Illuminate\Http\Request;
use App\Events\UserWasCreated;
use App\Http\Requests\UserRequest;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserUpdateRequest;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return datatables()
                ->eloquent(User::query())                
                ->addColumn('action', 'admin.users.partials.actions')
                ->rawColumns(['action'])
                ->toJson();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        $password = str_random(8);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => $password
        ]);

        UserWasCreated::dispatch($user, $password);

        $response = [
            'success'   => 'Se ha creado un usuario exitosamente',
            'codigo'  => '200',
            'user' => $user
        ];

        return $response;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        $response = [
            'user' => $user,
            'user_cars_relation' => $user->Cars,
            'code' => 200,
        ];

        return $response;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(UserUpdateRequest $request, User $user)
    {
        $user->update($request->validated());

        $response = [
            'success'   => 'Se ha actualizado el usuario exitosamente',
            'codigo'  => '200',
            'user' => $user
        ];

        return $response;

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        if($user === auth()->user()->id){
            $response = [
                'error'   => 'No te puedes eliminar a ti mismo',                
            ];

            return $response;
        } else {
            $user->delete(); 

            $response = [
                'success'   => 'Se ha eliminado el usuario exitosamente',
                'codigo'  => '200',
            ];
            
            return $response;
        }
    }
}
