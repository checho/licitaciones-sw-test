<?php

namespace App\Http\Controllers;

use App\Car;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests\CarRequest;
use App\Http\Requests\CarUpdateRequest;

class CarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {    
        return view('admin.cars.index');        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $users = User::all();
        // return view('admin.cars.create', compact('users'));

        $users = User::all();

        $response = [
            'users' => $users,
            'code' => 200
        ];

        return $response;
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CarRequest $request)
    {    
        // $car = Car::create($request->all());

        // return redirect()->route('cars.index')->with('info', 'El carro ha sido creado exitosamente');

        $car = Car::create($request->all());

        $response = [
            'success'   => 'Se ha creado un carro exitosamente',
            'codigo'  => '200',
            'car' => $car
        ];

        return $response;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Car  $car
     * @return \Illuminate\Http\Response
     */
    public function show(Car $car)
    {
        // return view('admin.cars.show', compact('car'));

        $response = [
            'car' => $car,
            'car_user_relation' => $car->User,
            'code' => 200,
        ];

        return $response;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Car  $car
     * @return \Illuminate\Http\Response
     */
    public function edit(Car $car)
    {        
        $users = User::where('id', '!=', $car->user_id)->get();

        // dd($users);
        // return view('admin.cars.edit', compact('car', 'users'));

        $response = [
            'users' => $users,
            'car' => $car,
            'relation' => $car->User,
            'code' => 200
        ];

        return $response;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Car  $car
     * @return \Illuminate\Http\Response
     */
    public function update(CarUpdateRequest $request, Car $car)
    {
        $car = $car->update($request->all());        

        // return redirect()->route('cars.index')->with('info', 'Se ha actualizado el carro exitosamente');

        $response = [
            'success'   => 'Se ha actualizado el carro exitosamente',
            'codigo'  => '200',
            'user' => $car
        ];

        return $response;

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Car  $car
     * @return \Illuminate\Http\Response
     */
    public function destroy(Car $car)
    {
        
        $car->delete();

        // return redirect()->back()->with('info', 'Se ha eliminado el carro exitosamente');

        $response = [
            'success' => 'Se ha eliminado el carro exitosamente',
            'code' => 200            
        ];

        return $response;
    }
}
