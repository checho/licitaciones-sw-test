<?php

use App\Car;
use App\User;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class CarsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        // following line retrieve all the user_ids from DB
        $users = User::all()->pluck('id');
        foreach(range(1,50) as $index){
            $car = Car::create([
                'car_brand' => $faker->word,                
                'car_model' => $faker->year($max = 'now'),
                'car_plate' => str_random(7),
                'car_seats' => $faker->randomElement($users),
                'user_id' => $faker->randomElement($users),
            ]);
        }
    }
}
