@component('mail::message')
# Hola {{ $user->name }}, hemos detectado que se a iniciado sesión desde otro lugar que no consideraste como seguro

<ul>
    <li>Navegador: {{ $dataAccess['browser'] }}</li>
    <li>Versión del navegador: {{ $dataAccess['browser_version'] }}</li>
    <li>Plataforma: {{ $dataAccess['platform'] }}</li>
    <li>Versión de la plataforma: {{ $dataAccess['platform_version'] }}</li>
    <li>Dispositivo: {{ $dataAccess['device'] }}</li>
    <li>Tipo de Dispositivo:  {{ $dataAccess['device_type'] }}</li>
    <li>Dirección ip: {{ $dataAccess['my_ip'] }}</li>
</ul>

@component('mail::button', ['url' => url('/')])
    Puedes informarte más viendo tu perfil.
@endcomponent

Gracias,<br>
{{ config('app.name') }}
@endcomponent
