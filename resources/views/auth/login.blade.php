<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 3 | Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="/adminlte/plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="/adminlte/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="/adminlte/dist/css/adminlte.min.css">
  <!-- SweetAlert2 -->
  <link rel="stylesheet" href="/adminlte/plugins/sweetalert2/sweetalert2.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <b>Admin</b>LTE
  </div>
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">Logueate para iniciar sesión</p>      
      <form method="POST" action="{{ route('login') }}" >
        @csrf
        <div class="input-group mb-3">          
          <input id="email" type="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="Correo Electrónico" required autofocus>
          @if ($errors->has('email'))
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $errors->first('email') }}</strong>
              </span>
          @else
              <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fas fa-envelope"></span>
                </div>
              </div>
          @endif          
        </div>
        <div class="input-group mb-3">          
          <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Contraseña" required>
          @if ($errors->has('password'))
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $errors->first('password') }}</strong>
              </span>
          @else
              <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fas fa-lock"></span>
                </div>
              </div>
          @endif          
        </div>
        <div class="row">
          <div class="col-8">
            <div class="icheck-primary">              
              <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
              <label for="remember">
                Remember Me
              </label>
            </div>
          </div>
          <!-- /.col -->
          <div class="col-6">
            <br>
            <button type="submit" class="btn btn-primary btn-block btn-flat">Iniciar sesión</button>
            <br>
          </div>
          <!-- /.col -->
        </div>
      </form>

      {{-- <div class="social-auth-links text-center mb-3">
        <p>- OR -</p>
        <a href="#" class="btn btn-block btn-primary">
          <i class="fab fa-facebook mr-2"></i> Sign in using Facebook
        </a>
        <a href="#" class="btn btn-block btn-danger">
          <i class="fab fa-google-plus mr-2"></i> Sign in using Google+
        </a>
      </div> --}}
      <!-- /.social-auth-links -->

      <p class="mb-1">
        {{-- <a href="{{ route('password.request') }}">Olvidaste tú contraseña ?</a> --}}
        <a href="#" data-toggle="modal" data-target="#forgotPassword">Olvidaste tú contraseña ?</a>
      </p>
      <p class="mb-0">
        <a href="{{ route('register') }}" class="text-center">Registrarme como nuevo miembro</a>
      </p>
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->

<div class="modal fade" id="forgotPassword">
    <div class="modal-dialog">
        <div class="modal-content bg-info">
            <div class="modal-header">
                <h4 class="modal-title">¿Olvidaste tu contraseña?</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" action="{{ route('password.email', '#modal-error') }}" aria-label="{{ __('Reset Password') }}">
                  @csrf                        
                    <label for="email_reset">{{ __('Correo electronico') }}</label>                            
                    <input id="email_reset" type="email" class="form-control {{ $errors->has('email_reset') ? ' is-invalid' : '' }}" name="email_reset" value="{{ old('email_reset') }}" required autofocus>
                    @if ($errors->has('email_reset'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email_reset') }}</strong>
                        </span>
                    @endif
                    <br>
                    <input type="submit" class="btn btn-outline-light" value="Enviar correo de confirmación">                    
                </form>
            </div>        
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-outline-light" data-dismiss="modal">Cancelar</button>
                                
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

  <!-- jQuery -->
  <script src="/adminlte/plugins/jquery/jquery.min.js"></script>
  <!-- Bootstrap 4 -->
  <script src="/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- SweetAlert2 -->
  <script src="/adminlte/plugins/sweetalert2/sweetalert2.min.js"></script>
  @include('admin.partials.sweetAlert.info')
  <script>
    $('form').on('submit', function(){
      $(this).find('input[type=submit]').attr('disabled', true);
    });
    
    if(window.location.hash === "#modal-error")
    {
      $('#forgotPassword').modal('show');
    }

      $('#forgotPassword').on('hide.bs.modal', function () {
          window.location.hash = "#"
      });

      $('#forgotPassword').on('shown.bs.modal', function () {
          $('#email_reset').focus()
          window.location.hash = "#modal-error"
      });    
  </script>
</body>
</html>
