@extends('layouts.admin')

@push('custom-styles')
    <!-- DataTables -->
    <link rel="stylesheet" href="/adminlte/plugins/datatables/dataTables.bootstrap4.css">
    <!-- SweetAlert2 -->
    <link rel="stylesheet" href="/adminlte/plugins/sweetalert2/sweetalert2.min.css">
@endpush

@section('header')
    <div class="col-sm-6">
        <h1 class="m-0 text-dark">Módulo Usuarios</h1>
    </div><!-- /.col -->
    <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href=""><i class="nav-icon fas fa-tachometer-alt"></i> Admin</a></li>
        <li class="breadcrumb-item active">Mod - Usuarios</li>
        </ol>
    </div><!-- /.col -->
@endsection

@section('content')
    <div class="col-md-12">
        <section class="content">
            <!-- Default box -->
            <div class="card card-primary card-outline">
            <div class="card-header">                
                <h3 class="card-title float-left">Visualización de usuarios</h3>                                
                <a class="btn btn-outline-success float-right" href="javascript:void(0)" id="createNewUser"> Crear usuario <i class="fas fa-plus"></i></a>
            </div>
            
            <div class="card-body">
                <table id="users-table" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th width="20px">ID</th>
                            <th>Nombre</th>
                            <th>Email</th>
                            <th width="100px">Acciones</th>                            
                        </tr>
                    </thead>                    
                    <tfoot>
                        <tr>
                            <th width="20px">ID</th>
                            <th>Nombre</th>
                            <th>Email</th>
                            <th width="100px">Acciones</th>                            
                        </tr>
                    </tfoot>
                </table>                
            </div>
            <!-- /.card-body -->
            <div class="card-footer">            
                <a href="{{ URL::previous() }}" class="btn btn-outline-secondary float-right">Regresar <i class="fas fa-arrow-alt-circle-left"></i></a>
            </div>
            <!-- /.card-footer-->
            </div>
            <!-- /.card -->  
        </section>
        <!-- /.content -->
    </div>
    
@endsection

@push('custom-scripts')

    <!-- DataTables -->
    <script src="/adminlte/plugins/datatables/jquery.dataTables.js"></script>
    <script src="/adminlte/plugins/datatables/dataTables.bootstrap4.js"></script>
    <!-- SweetAlert2 -->
    <script src="/adminlte/plugins/sweetalert2/sweetalert2.min.js"></script>
    @include('admin.partials.sweetAlert.info')    
    @include('admin.partials.sweetAlert.error')
    @include('admin.users.partials.create_user_modal')
    @include('admin.users.partials.edit_user_modal')    
    @include('admin.users.partials.view_user_modal')    
    <script>
        $(function () {

            const Toast = Swal.mixin({
				toast: true,
				position: 'top-end',
				showConfirmButton: false,
				timer: 5000
			});           

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            // INDEX -------------------------------

            let table = $('#users-table').DataTable({
                processing  : true,
                serverSide  : true,
                paging      : true,
                searching   : true,
                ordering    : true,
                autoWidth   : false,
                scrollX     : true,                
                ajax: "{{ url('api/users') }}",
                columns: [
                        {data: 'id', name: 'id'},
                        {data: 'name', name: 'name'},
                        {data: 'email', name: 'email'},
                        {data: 'action', name: 'action'},                                           
                    ],
                "language": {
                    "sProcessing": "Procesando...",
                    "sLengthMenu": "Mostrar _MENU_ registros",
                    "sZeroRecords": "No se encontraron resultados",
                    "sEmptyTable": "Ningún dato disponible en esta tabla",
                    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix": "",
                    "sSearch": "Buscar:",
                    "sUrl": "",
                    "sInfoThousands": ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst": "Primero",
                        "sLast": "Último",
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }                
            });

            // INDEX -------------------------------
            // STORE -------------------------------

            $('#createNewUser').click(function () {
                $('#saveBtn').val("Crear usuario");
                $('#saveBtn').attr("disabled", false);
                $('#saveBtn').html("Guardar cambios");
                $('#userForm').trigger("reset");                
                $('#modelHeading').html("Crear nuevo usuario");
                $('#ajaxModel').modal('show');
            });

            $('#saveBtn').click(function (e) {
                e.preventDefault();
                $(this).val('Enviando..');                
                $(this).attr('disabled', true);

                $.ajax({
                    data: $('#userForm').serialize(),
                    url: "{{ url('api/users') }}",
                    type: "POST",
                    dataType: 'json',
                    success: function (data) {
                        Toast.fire({
                            type: 'success',
                            title: `${data.success}`
			            });
                        $('#userForm').trigger("reset");
                        $('#ajaxModel').modal('hide');                        
                        table.draw();
                    },
                    error: function (data) {                        
                        $('#saveBtn').val("Crear usuario");
                        $('#saveBtn').attr('disabled', false);
                        $.each(data.responseJSON.errors, function (i, value) {
                            if (i === document.getElementById(`${i}`).id) {
                                $(`#${i}`).addClass('is-invalid');
                                $.each(value, function (j, value) {                                    
                                    document.getElementById(`error-${i}`).innerHTML = `<strong>${value}</strong>`;
                                })
                            } else {
                                $(`#${i}`).removeClass('is-invalid');
                            }
                        });
                    }
                });
            });

            // STORE -------------------------------
            // SHOW --------------------------------

            $('body').on('click', '.viewUser', function () {
                var user_id = $(this).data('id');
                $.get("{{ url('api/users') }}" + '/' + user_id , function (data) {                    
                    $('#view-modelHeading').html("Usuario: " + data.user.name);
                    $('#saveEditBtn').val("Editar usuario");
                    $('#saveEditBtn').attr("disabled", false);
                    $('#saveEditBtn').attr('data-id', data.user.id);
                    $('#viewUser').modal('show');
                    $('#view-email').html(data.user.email);
                    $('#view-member_since').html(data.user.created_at);                    
                    if (data.user_cars_relation.length === 0) {                        
                        $('#user_cars_relation').html(`<strong>No se registraron carros para este usuario. Si quieres registrar un nuevo carro has clic <a href="{{ route('cars.index') }}" target="_blank">aquí</a></strong>`);                        
                    } else {
                        let template = ``;
                        $.each(data.user_cars_relation, function (i, value) {                            
                            template += `<span><strong>Id carro: ${value.id} - </strong></span><span><a href="{{ route('cars.index') }}" target="_blank">${value.car_brand}</a></span><br>`                                                             
                        })
                        $('#user_cars_relation').html(template);
                    }                    
                })
            });

            // SHOW --------------------------------
            // UPDATE ------------------------------

            $('body').on('click', '.editUser', function () {
                var user_id = $(this).data('id');
                $.get("{{ route('users.index') }}" + '/' + user_id + '/edit', function (data) {
                    $('#edit-modelHeading').html("Editar usuario");
                    $('#saveEditBtn').val("Editar usuario");
                    $('#saveEditBtn').attr("disabled", false);
                    $('#saveEditBtn').attr('data-id', data.id);
                    $('#editUser').modal('show');                    
                    $('#edit_id').val(data.id);                    
                    $('#edit_name').val(data.name);                                       
                    $('#edit_email').val(data.email);                    
                })
            });

            $('#saveEditBtn').click(function (e) {
                e.preventDefault();
                $(this).val('Enviando..');
                $('#saveEditBtn').attr("disabled", true);
            
                var user_id = $(this).data('id');
                $.ajax({
                    data: $('#userEditForm').serialize(),
                    url: "{{ url('api/users') }}" + '/' + user_id,
                    type: "PUT",
                    dataType: 'json',
                    success: function (data) {
                        $('#editUser').modal('hide');
                        Toast.fire({
                            type: 'success',
                            title: `${data.success}`
			            });                                                
                        table.draw();
                    },
                    error: function (data) {                        
                        $('#saveEditBtn').val("Editar usuario");
                        $('#saveEditBtn').attr('disabled', false);
                        $.each(data.responseJSON.errors, function (i, value) {                                                        
                            let editPrefix = 'edit_'+i;
                            if (editPrefix === document.getElementById(editPrefix).id) {                                                                
                                $(`#${editPrefix}`).addClass('is-invalid');
                                $.each(value, function (j, value) {
                                    let editPrefix2 = 'error_' + editPrefix;                                                                       
                                    document.getElementById(editPrefix2).innerHTML = `<strong>${value}</strong>`;
                                })
                            } else {
                                $(`#${editPrefix}`).removeClass('is-invalid');
                            }
                        });
                    }
                });
            });

            // UPDATE ------------------------------
            // DELETE ------------------------------

            $('body').on('click', '.deleteUser', function () {

                var user_id = $(this).data("id");

                Swal.fire({
                    title: '¿Estas seguro de eliminarlo?',
                    text: "Recuerda que este usuario puede estar asociado a varios carros. Los carros tambien serán eliminados",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Si, eliminar'
                }).then((result) => {
                    if (result.value) {
                        jQuery.ajax({
                            url: '{{ url('admin/users') }}' + '/' + user_id,
                            method: 'DELETE',
                            data: {
                                id: user_id
                            },
                            success: function (result) {
                                if (result.error) {
                                    Swal.fire(
                                        'Error!',
                                        `${result.error}`,
                                        'error'
                                    )
                                } else {
                                    Swal.fire(
                                        'Eliminado!',
                                        `${result.success}`,
                                        'success'
                                    )
                                    table.draw();
                                }
                            },
                            error: function (data) {
                                console.log(data);
                            }
                        });
                    }
                });
            });

            // DELETE ------------------------------

        });
    </script>           

@endpush