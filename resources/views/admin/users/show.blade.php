@extends('layouts.admin')

@push('custom-styles')
    
@endpush

@section('header')
    <div class="col-sm-6">
        <h1 class="m-0 text-dark">Módulo Usuarios</h1>
    </div><!-- /.col -->
    <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href=""><i class="nav-icon fas fa-tachometer-alt"></i> Admin</a></li>
        <li class="breadcrumb-item"><a href="{{ route('users.index') }}"><i class=" fas fa-users"></i> Mod - Usuarios</a></li>
        <li class="breadcrumb-item active">Usuario - {{ $user->name }}</li>
        </ol>
    </div><!-- /.col -->
@endsection

@section('content')
    <div class="col-md-12">
        <section class="content">
            <!-- Default box -->
            <div class="card card-primary card-outline">
            <div class="card-header">                
                <h3 class="card-title float-left">Visualización de usuario - {{ $user->id }}</h3>                
                <a class="btn btn-outline-success float-right" href="{{ route('users.edit', $user) }}">Editar usuario <i class="fas fa-user-edit"></i></a>
                    {{-- <button class="btn btn-success btn-outline" data-toggle="modal" data-target="#addBtn">Crear usuario <i class="fas fa-plus"></i></button> --}}
                    {{-- @include('admin.users.partials.create_user_modal') --}}                    
            </div>
            
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <h3>Email:</h3>
                        <div class="col-md-11 offset-1">
                            <strong>{{ $user->email }}</strong>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <h3>Miembro desde:</h3>
                        <div class="col-md-11 offset-1">
                            <strong>{{ $user->created_at->toFormattedDateString() }}</strong>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <h3>Carros que maneja este usuario:</h3>
                        <div class="col-md-11 offset-1">
                            @forelse ($user->Cars as $car)
                                <span><strong>Id carro: {{ $car->id }} - </strong></span><span><a href="{{ route('cars.show', $car) }}">{{ $car->car_brand }}</a></span><br>
                                @unless($loop->last)
                                    <hr>
                                @endunless
                            @empty
                                <div class="col-md-12 offset-1">
                                    <strong>No se registraron carros para este usuario. Si quieres registrar un nuevo carro has clic <a href="{{ route('cars.create') }}">aquí</a></strong>
                                </div>
                            @endforelse                            
                        </div>
                    </div>
                      
                </div>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">            
                <a href="{{ route('users.index') }}" class="btn btn-outline-secondary float-right">Regresar <i class="fas fa-arrow-alt-circle-left"></i></a>
            </div>
            <!-- /.card-footer-->
            </div>
            <!-- /.card -->  
        </section>
        <!-- /.content -->
    </div>
    
@endsection

@push('custom-scripts')    

@endpush