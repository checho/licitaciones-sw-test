@extends('layouts.admin')

@push('custom-styles')
    <!-- SweetAlert2 -->
    <link rel="stylesheet" href="/adminlte/plugins/sweetalert2/sweetalert2.min.css">
@endpush

@section('header')
    <div class="col-sm-6">
        <h1 class="m-0 text-dark">Módulo Usuarios</h1>
    </div><!-- /.col -->
    <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}"><i class="nav-icon fas fa-tachometer-alt"></i> Admin</a></li>
        <li class="breadcrumb-item"><a href="{{ route('users.index') }}"><i class=" fas fa-users"></i> Mod - Usuarios</a></li>
        <li class="breadcrumb-item active">Editar usuario - {{ $user->name }}</li>
        </ol>
    </div><!-- /.col -->
@endsection

@section('content')
    <div class="col-md-12">
        <section class="content">
            <!-- Default box -->
            <div class="card card-warning card-outline">
            <div class="card-header">
                <h3 class="card-title float-left">Edición de usuario</h3>

                <a href="{{ route('users.index') }}" class="btn btn-outline-secondary float-right">Regresar <i class="fas fa-arrow-alt-circle-left"></i></a>                
            </div>
            <div class="card-body">
                <form action="{{ route('users.update', $user) }}" method="post">
                    @include('admin.users.partials.editForm')
                </form>                
            </div>
            <!-- /.card-body -->
            {{-- <div class="card-footer">            
                
            </div> --}}
            <!-- /.card-footer-->
            </div>
            <!-- /.card -->  
        </section>
        <!-- /.content -->
    </div>
@endsection

@push('custom-scripts')
    <!-- SweetAlert2 -->
    <script src="/adminlte/plugins/sweetalert2/sweetalert2.min.js"></script>    
    @include('admin.partials.sweetAlert.error')
@endpush