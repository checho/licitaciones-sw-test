<div id="ajaxModel" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modelHeading"></h5>
                
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="userForm" name="userForm">
                    <input type="hidden" name="user_id" id="user_id">                
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="name">Nombre del usuario</label>
                            <input id="name" class="form-control " type="text" name="name" value="{{ old('name') }}" autofocus>                            
                                <span id="error-name" class="invalid-feedback" role="alert">
                                    
                                </span>
                            
                        </div>
                        <div class="form-group col-md-6">
                            <label for="Email">Email del usuario</label>
                            <input id="email" class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" type="email" name="email" value="{{ old('email') }}">                            
                                <span id="error-email" class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>                               
                        </div>
                        <div class="col-md-12">
                            <span class="help-block">La contraseña será generada y enviada al nuevo usuario via email</span>                            
                        </div>
                        <div class="col-md-12">
                            <hr>                            
                            <input type="submit" class="btn btn-success" id="saveBtn" value="create">
                            <button type="button" class="btn btn-default waves-effect float-right" data-dismiss="modal">Cancelar</button>
                        </div>      
                    </div>
                </form>
            </div>            
        </div>
        <!-- /.modal-content -->
    </div>
</div>

@push('custom-scripts')    

    {{-- custom scripts --}}

@endpush