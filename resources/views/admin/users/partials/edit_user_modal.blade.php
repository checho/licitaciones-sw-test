<div id="editUser" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="edit-modelHeading"></h5>
                
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="userEditForm" name="userEditForm">
                    <input type="hidden" name="id" id="edit_id">             
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="name">Nombre del usuario</label>
                            <input id="edit_name" class="form-control " type="text" name="name" value="{{ old('name') }}" autofocus>                            
                                <span id="error_edit_name" class="invalid-feedback" role="alert">                                    
                                </span>
                            
                        </div>
                        <div class="form-group col-md-6">
                            <label for="Email">Email del usuario</label>
                            <input id="edit_email" class="form-control " type="email" name="email" value="{{ old('email') }}">                            
                                <span id="error_edit_email" class="invalid-feedback" role="alert">                                    
                                </span>                               
                        </div>
                        <div class="form-group col-md-6">
                            <label for="password">Contraseña</label>
                            <input id="edit_password" class="form-control " type="password" name="password">                        
                            <span id="error_edit_password" class="invalid-feedback" role="alert">
                            </span>
                            <span class="help-block">Dejar en blanco si no quieres cambiar la contraseña</span>                           
                            
                        </div>        
                        <div class="form-group col-md-6">
                            <label for="password_confirmation">Confirmar contraseña</label>
                            <input class="form-control" type="password" name="password_confirmation">                            
                        </div>
                        <div class="col-md-12">                            
                            <input type="submit" class="btn btn-success" id="saveEditBtn" value="edit">
                            <button type="button" class="btn btn-default waves-effect float-right" data-dismiss="modal">Cancelar</button>
                        </div>      
                    </div>
                </form>
            </div>            
        </div>
        <!-- /.modal-content -->
    </div>
</div>

@push('custom-scripts')    

   {{-- custom scripts --}}

@endpush