@csrf @method('PUT')

<div class="row">
    <div class="form-group col-md-6">
        <label for="name">Nombre del usuario</label>
        <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" type="text" name="name" value="{{ old('name', $user->name) }}" autofocus>
        @if ($errors->has('name'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>
    <div class="form-group col-md-6">
        <label for="Email">Email del usuario</label>
        <input class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" type="email" name="email" value="{{ old('email', $user->email) }}">
        @if ($errors->has('email'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
    </div>
    <div class="form-group col-md-6">
        <label for="password">Contraseña</label>
        <input class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}" type="password" name="password">
        <span class="help-block">Dejar en blanco si no quieres cambiar la contraseña</span>
        @if ($errors->has('password'))
        <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('password') }}</strong>
        </span>
        @endif
    </div>        
    <div class="form-group col-md-6">
        <label for="password_confirmation">Confirmar contraseña</label>
        <input class="form-control {{ $errors->has('password_confirmation') ? 'is-invalid' : '' }}" type="password" name="password_confirmation">
        @if ($errors->has('password_confirmation'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('password_confirmation') }}</strong>
            </span>
        @endif
    </div>    

    <div class="col-md-12">
            <button type="submit" class="btn btn-outline-success float-left">Terminar <i class="fas fa-check"></i></button>
            <a href="{{ route('users.index') }}" class="btn btn-outline-secondary float-right">Regresar <i class="fas fa-arrow-alt-circle-left"></i></a>                
    </div>
</div>


