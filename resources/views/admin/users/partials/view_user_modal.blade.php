<div id="viewUser" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="view-modelHeading"></h5>
                
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                {{-- <form id="userEditForm" name="userEditForm">
                    <input type="hidden" name="id" id="user-id">             
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="name">Nombre del usuario</label>
                            <input id="user-name" class="form-control " type="text" name="name" value="{{ old('name') }}" autofocus>                            
                                <span id="edit-error-name" class="invalid-feedback" role="alert">                                    
                                </span>
                            
                        </div>
                        <div class="form-group col-md-6">
                            <label for="Email">Email del usuario</label>
                            <input id="user-email" class="form-control " type="email" name="email" value="{{ old('email') }}">                            
                                <span id="edit-error-email" class="invalid-feedback" role="alert">                                    
                                </span>                               
                        </div>
                        <div class="form-group col-md-6">
                            <label for="password">Contraseña</label>
                            <input id="user-password" class="form-control " type="password" name="password">                        
                            <span id="edit-error-password" class="invalid-feedback" role="alert">
                            </span>
                            <span class="help-block">Dejar en blanco si no quieres cambiar la contraseña</span>                           
                            
                        </div>        
                        <div class="form-group col-md-6">
                            <label for="password_confirmation">Confirmar contraseña</label>
                            <input class="form-control" type="password" name="password_confirmation">                            
                        </div>
                        <div class="col-md-12">                            
                            <input type="submit" class="btn btn-success" id="saveEditBtn" value="edit">
                            <button type="button" class="btn btn-default waves-effect float-right" data-dismiss="modal">Cancelar</button>
                        </div>      
                    </div>
                </form> --}}

                
                <div class="row">
                    <div class="col-md-6">
                        <h3>Email:</h3>
                        <div class="col-md-11 offset-1">
                            <strong id="view-email"></strong>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <h3>Miembro desde:</h3>
                        <div class="col-md-11 offset-1">
                            {{-- <strong id="member_since" >{{ $user->created_at->toFormattedDateString() }}</strong> --}}
                            <strong id="view-member_since" ></strong>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <h3>Carros que maneja este usuario:</h3>
                        <div id="user_cars_relation" class="col-md-11 offset-1">
                                                        
                        </div>
                    </div>
                    <div class="col-md-12">
                        <hr>
                        <button type="button" class="btn btn-default waves-effect float-right" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>

            </div>            
        </div>
        <!-- /.modal-content -->
    </div>
</div>

@push('custom-scripts')    

   {{-- custom scripts --}}

@endpush