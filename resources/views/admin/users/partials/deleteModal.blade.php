<div class="modal fade" id="deleteUser-{{ $user->id }}">
    <div class="modal-dialog">
        <div class="modal-content bg-danger">
            <div class="modal-header">
                <h4 class="modal-title">¿Deseas eliminar este usuario?</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <strong>¿Estas seguro de querer eliminarlo?</strong>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-outline-light" data-dismiss="modal">Cancelar</button>
                <form action="{{ route('users.destroy', $user) }}" method="post">
                    @csrf @method('DELETE')
                    <button type="submit" class="btn btn-outline-light">Si, eliminar</button>
                </form>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->