@if(Session::has('info'))		
	<script>
		$(function () { 
			const Toast = Swal.mixin({
				toast: true,
				position: 'top-end',
				showConfirmButton: false,
				timer: 5000
			});

			Toast.fire({
				type: 'success',
				title: '{{ Session::get('info') }}'
			});
		});
		$('#forgotPassword').modal('hide');
		window.location.hash = "#"
	</script>
@endif