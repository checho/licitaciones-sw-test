@if(Session::has('warning'))
    <script>
        $(function () { 
            const Toast = Swal.mixin({                
                position: 'top-center',
                showConfirmButton: true                
            });

            Toast.fire({
                type: 'warning',
                html:
                'Puedes ver más <b>visitando tú</b>, ' +
                '<a href="#">Perfil</a> ',                
                title: '{{ Session::get('warning') }}'
            });
        });
    </script>
@endif