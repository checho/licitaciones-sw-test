@if(count($errors))	
    <script>
		$(function () { 
			const Toast = Swal.mixin({
				toast: true,
				position: 'top-end',
				showConfirmButton: false,
				timer: 5000
			});

			Toast.fire({
				type: 'error',
				title: 'Ups, algo salío mal, por favor revisa'
			});
		});
	</script>
@endif

@if(Session::has('error'))
    <script>
        $(function () { 
            const Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 5000
            });

            Toast.fire({
                type: 'error',
                title: '{{ Session::get('error') }}'
            });
        });
    </script>
@endif
