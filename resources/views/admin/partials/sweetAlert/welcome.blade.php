@if(Session::has('welcome'))		
	<script>
		$(function () { 
			const Toast = Swal.mixin({
				toast: true,
				position: 'top-end',
				showConfirmButton: false,
				timer: 5000
			});

			Toast.fire({
				type: 'success',
				title: '{{ Session::get('welcome') }}'
			});
		});
		
	</script>
@endif

