@extends('layouts.admin')

@push('custom-styles')
    <!-- SweetAlert2 -->
    <link rel="stylesheet" href="/adminlte/plugins/sweetalert2/sweetalert2.min.css">
@endpush

@section('header')    

    <div class="col-sm-6">        

        @if(Session::has('authenticated'))
            {{Session::get('authenticated')}}
        @endif

    </div><!-- /.col -->
    {{-- <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="">Home</a></li>
        <li class="breadcrumb-item active">Starter page</li>
        </ol>
    </div><!-- /.col --> --}}
@endsection

@section('content')
<div class="modal fade" id="saveIp">
    <div class="modal-dialog">
        <div class="modal-content bg-warning">
            <div class="modal-header">
                <h4 class="modal-title">¿Consideras que estas navegando desde un lugar seguro?</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>            
            <div class="modal-footer justify-content-between">
                <form id="ip-form" method="POST" action="{{ route('save.data') }}" style="display: none;">
                  @csrf                                                                
                </form>
                <button type="button" class="btn btn-outline-light" data-dismiss="modal">No lo considero</button>                
                <button onclick="event.preventDefault(); $(this).attr('disabled', true); document.getElementById('ip-form').submit();" class="btn btn-outline-light">Si, lo considero</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
@endsection

@push('custom-scripts')
    <!-- SweetAlert2 -->
    <script src="/adminlte/plugins/sweetalert2/sweetalert2.min.js"></script>
    @include('admin.partials.sweetAlert.welcome')
    @include('admin.partials.sweetAlert.info')
    @include('admin.partials.sweetAlert.warning')
    @include('admin.partials.sweetAlert.error')

    @if(Session::has('userNoHasIp'))
        <script>
            $('#saveIp').modal('show');
        </script>
    @endif
@endpush