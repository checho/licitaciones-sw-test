@extends('layouts.admin')

@push('custom-styles')
    <!-- SweetAlert2 -->
    <link rel="stylesheet" href="/adminlte/plugins/sweetalert2/sweetalert2.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="/adminlte/plugins/select2/css/select2.min.css">
    <style>
        .has-error .select2-selection {
            border: 1px solid #dc3545;
            border-radius: 4px;
            border-color:rgb(185, 74, 72) !important;
        }
    </style>
@endpush

@section('header')
    <div class="col-sm-6">
        <h1 class="m-0 text-dark">Módulo Carros</h1>
    </div><!-- /.col -->
    <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}"><i class="nav-icon fas fa-tachometer-alt"></i> Admin</a></li>
        <li class="breadcrumb-item"><a href="{{ route('cars.index') }}"><i class=" fas fa-car"></i> Mod - Carros</a></li>
        <li class="breadcrumb-item active">Editar carro - {{ $car->id }}</li>
        </ol>
    </div><!-- /.col -->
@endsection

@section('content')
    <div class="col-md-12">
        <section class="content">
            <!-- Default box -->
            <div class="card card-warning card-outline">
            <div class="card-header">
                <h3 class="card-title float-left">Edición de carro</h3>

                <a href="{{ route('cars.index') }}" class="btn btn-outline-secondary float-right">Regresar <i class="fas fa-arrow-alt-circle-left"></i></a>                
            </div>
            <div class="card-body">
                <form action="{{ route('cars.update', $car) }}" method="post">
                    @include('admin.cars.partials.editForm')
                </form>                
            </div>
            <!-- /.card-body -->
            {{-- <div class="card-footer">            
                
            </div> --}}
            <!-- /.card-footer-->
            </div>
            <!-- /.card -->  
        </section>
        <!-- /.content -->
    </div>
@endsection

@push('custom-scripts')
    <!-- SweetAlert2 -->
    <script src="/adminlte/plugins/sweetalert2/sweetalert2.min.js"></script>
    @include('admin.partials.sweetAlert.error')
    <!-- Select2 -->
    <script src="/adminlte/plugins/select2/js/select2.full.min.js"></script>

    <script>
        $(function () {
            //Initialize Select2 Elements
            $('.select2-users').select2()
        })
    </script>
@endpush