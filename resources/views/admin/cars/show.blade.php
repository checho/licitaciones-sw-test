@extends('layouts.admin')

@push('custom-styles')
    
@endpush

@section('header')
    <div class="col-sm-6">
        <h1 class="m-0 text-dark">Módulo Carrros</h1>
    </div><!-- /.col -->
    <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href=""><i class="nav-icon fas fa-tachometer-alt"></i> Admin</a></li>
        <li class="breadcrumb-item"><a href="{{ route('cars.index') }}"><i class=" fas fa-car"></i> Mod - Carros</a></li>
        <li class="breadcrumb-item active">Carro - {{ $car->name }}</li>
        </ol>
    </div><!-- /.col -->
@endsection

@section('content')
    <div class="col-md-12">
        <section class="content">
            <!-- Default box -->
            <div class="card card-primary card-outline">
            <div class="card-header">                
                <h3 class="card-title float-left">Visualización de carro - {{ $car->id }}</h3>                
                <a class="btn btn-outline-success float-right" href="{{ route('cars.edit', $car) }}">Editar carro <i class="fas fa-car"></i></a>
                    {{-- <button class="btn btn-success btn-outline" data-toggle="modal" data-target="#addBtn">Crear usuario <i class="fas fa-plus"></i></button> --}}
                    {{-- @include('admin.cars.partials.create_car_modal') --}}                    
            </div>
            
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <h3>Persona que conduce este carro:</h3>
                        <div class="col-md-11 offset-1">
                            <strong><a href="{{ route('users.show', $car->User->id) }}">{{ $car->User->name }}</a> - {{ $car->User->email }}</strong>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <h3>Marca del carro:</h3>
                        <div class="col-md-11 offset-1">
                            <strong>{{ $car->car_brand }}</strong>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <h3>Placa del carro:</h3>
                        <div class="col-md-11 offset-1">
                            <strong>{{ $car->car_plate }}</strong>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <h3>Modelo del carro:</h3>
                        <div class="col-md-11 offset-1">
                            <strong>{{ $car->car_model }}</strong>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <h3>Número de asientos del carro:</h3>
                        <div class="col-md-11 offset-1">
                            <strong>{{ $car->car_seats}}</strong>
                        </div>
                    </div>
                      
                </div>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">            
                <a href="{{ route('cars.index') }}" class="btn btn-outline-secondary float-right">Regresar <i class="fas fa-arrow-alt-circle-left"></i></a>
            </div>
            <!-- /.card-footer-->
            </div>
            <!-- /.card -->  
        </section>
        <!-- /.content -->
    </div>
    
@endsection

@push('custom-scripts')    

@endpush