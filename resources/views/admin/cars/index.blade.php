@extends('layouts.admin')

@push('custom-styles')
    <!-- DataTables -->
    <link rel="stylesheet" href="/adminlte/plugins/datatables/dataTables.bootstrap4.css">
    <!-- SweetAlert2 -->
    <link rel="stylesheet" href="/adminlte/plugins/sweetalert2/sweetalert2.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="/adminlte/plugins/select2/css/select2.min.css">
@endpush

@section('header')
    <div class="col-sm-6">
        <h1 class="m-0 text-dark">Módulo Carros</h1>
    </div><!-- /.col -->
    <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href=""><i class="nav-icon fas fa-tachometer-alt"></i> Admin</a></li>
        <li class="breadcrumb-item active">Mod - Carros</li>
        </ol>
    </div><!-- /.col -->
@endsection

@section('content')
    <div class="col-md-12">
        <section class="content">
            <!-- Default box -->
            <div class="card card-primary card-outline">
            <div class="card-header">                
                <h3 class="card-title float-left">Visualización de carros</h3>                
                <a class="btn btn-outline-success float-right" href="javascript:void(0)" id="createNewCar"> Crear Carro <i class="fas fa-plus"></i></a>                  
            </div>
            
            <div class="card-body">
                <table id="cars-table" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th width="20px">ID</th>
                            <th>Marca</th>
                            <th>Modelo</th>
                            <th>Placa</th>
                            <th>Numero de asientos</th>
                            <th width="100px">Acciones</th>                        
                        </tr>
                    </thead>
                    {{-- <tbody>
                        @foreach ($cars as $car)                            
                            <tr>
                                <td>{{ $car->id }}</td>
                                <td>{{ $car->car_brand }}</td>
                                <td>{{ $car->car_model }}</td>
                                <td>{{ $car->car_plate }}</td>
                                <td>{{ $car->car_seats }}</td>                                
                                <td>
                                    <div class="btn-group float-right">                                            
                                        <a class="btn btn-outline-info" href="{{ route('cars.show', $car) }}" ><i class="fas fa-align-left fa-eye"></i></a>                                            
                                        <a class="btn btn-outline-warning" href="{{ route('cars.edit', $car) }}" ><i class="fas fa-edit"></i></a>                                                                                    
                                        <a class="btn btn-outline-danger" href="#" data-toggle="modal" data-target="#deleteCar-{{ $car->id }}"><i class="fa fa-trash"></i></a>
                                    </div>                                    
                                </td>                                
                            </tr>
                            @include('admin.cars.partials.deleteModal')
                        @endforeach
                    </tbody> --}}
                    <tfoot>
                        <tr>
                            <th width="20px">ID</th>
                            <th>Marca</th>
                            <th>Modelo</th>
                            <th>Placa</th>
                            <th>Numero de asientos</th>
                            <th width="100px">Acciones</th> 
                        </tr>
                    </tfoot>
                </table>
                
            </div>
            <!-- /.card-body -->
            <div class="card-footer">            
                <a href="{{ URL::previous() }}" class="btn btn-outline-secondary float-right">Regresar <i class="fas fa-arrow-alt-circle-left"></i></a>
            </div>
            <!-- /.card-footer-->
            </div>
            <!-- /.card -->  
        </section>
        <!-- /.content -->
    </div>
    
@endsection

@push('custom-scripts')

    <!-- DataTables -->
    <script src="/adminlte/plugins/datatables/jquery.dataTables.js"></script>
    <script src="/adminlte/plugins/datatables/dataTables.bootstrap4.js"></script>
    <!-- SweetAlert2 -->
    <script src="/adminlte/plugins/sweetalert2/sweetalert2.min.js"></script>
    <!-- Select2 -->
    <script src="/adminlte/plugins/select2/js/select2.full.min.js"></script>
    @include('admin.partials.sweetAlert.info')    
    @include('admin.partials.sweetAlert.error')    
    @include('admin.cars.partials.create_car_modal')
    @include('admin.cars.partials.edit_car_modal')    
    @include('admin.cars.partials.view_car_modal')    
    <script>
        $(function () {

            const Toast = Swal.mixin({
				toast: true,
				position: 'top-end',
				showConfirmButton: false,
				timer: 5000
			});

            $(function () {
                //Initialize Select2 Elements
                $('.select2-users').select2()
            })           

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            // INDEX -------------------------------

            let table = $('#cars-table').DataTable({
                processing  : true,
                serverSide  : true,
                paging      : true,
                searching   : true,
                ordering    : true,
                autoWidth   : false,
                scrollX     : true,
                ajax: "{{ url('api/cars') }}",
                columns: [
                        {data: 'id', name: 'id'},
                        {data: 'car_brand', name: 'car_brand'},
                        {data: 'car_model', name: 'car_model'},
                        {data: 'car_plate', name: 'car_plate'},
                        {data: 'car_seats', name: 'car_seats'},                        
                        {data: 'action', name: 'action'},                                           
                    ],
                "language": {
                    "sProcessing": "Procesando...",
                    "sLengthMenu": "Mostrar _MENU_ registros",
                    "sZeroRecords": "No se encontraron resultados",
                    "sEmptyTable": "Ningún dato disponible en esta tabla",
                    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix": "",
                    "sSearch": "Buscar:",
                    "sUrl": "",
                    "sInfoThousands": ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst": "Primero",
                        "sLast": "Último",
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
            });

            // INDEX -------------------------------
            // STORE -------------------------------            

            $('body').on('click', '#createNewCar', function () {            
                $.get("{{ route('cars.create') }}", function (data) {
                    $('#saveBtn').attr("disabled", false);
                    $('#saveBtn').val("Guardar cambios");
                    $('#carForm').trigger("reset");                
                    $('#modelHeading').html("Crear nuevo carro");
                    $('#ajaxModel').modal('show');                    
                                      
                    if (data.users.length === 0) {                        
                        document.getElementById('user_id').innerHTML = `<option value="">No hay usuarios</option>`;
                    } else {
                        let template = `<option value="" selected>Asigne un usuario para que conduzca este carrro</option>`;
                        $.each(data.users, function (i, value) {                            
                             template += `<option value="${value.id}">${value.name}</option>`;                            
                        })
                            document.getElementById('user_id').innerHTML = template;
                    }                    
                })
            });

            $('#saveBtn').click(function (e) {
                e.preventDefault();
                $(this).val('Enviando..');                
                $(this).attr('disabled', true);

                $.ajax({
                    data: $('#carForm').serialize(),
                    url: "{{ route('cars.store') }}",
                    type: "POST",
                    dataType: 'json',
                    success: function (data) {
                        Toast.fire({
                            type: 'success',
                            title: `${data.success}`
			            });
                        $('#carForm').trigger("reset");
                        $('#ajaxModel').modal('hide');                        
                        table.draw();
                    },
                    error: function (data) {                        
                        $('#saveBtn').val("Crear carro");
                        $('#saveBtn').attr('disabled', false);
                        $.each(data.responseJSON.errors, function (i, value) {
                            if (i === document.getElementById(`${i}`).id) {
                                $(`#${i}`).addClass('is-invalid');
                                $.each(value, function (j, value) {                                    
                                    document.getElementById(`error-${i}`).innerHTML = `<strong>${value}</strong>`;
                                })
                            } else {
                                $(`#${i}`).removeClass('is-invalid');
                            }
                        });
                    }
                });
            });

            // STORE -------------------------------
            // SHOW --------------------------------

            $('body').on('click', '.viewCar', function () {
                var car_id = $(this).data('id');
                $.get("{{ route('cars.index') }}" + '/' + car_id , function (data) {
                    $('#view-modelHeading').html("Carro: " + data.car.id);
                    $('#saveEditBtn').val("Editar usuario");
                    $('#saveEditBtn').attr("disabled", false);
                    $('#saveEditBtn').attr('data-id', data.car.id);
                    $('#viewCar').modal('show');
                    $('#view-car_brand').html(data.car.car_brand);
                    $('#view-car_model').html(data.car.car_model);
                    $('#view-car_plate').html(data.car.car_plate);
                    $('#view-car_seats').html(data.car.car_seats);                                                                                                                                  
                    $('#car_user_relation').html(`<strong><a href="{{ route('users.index') }}" target="_blank">${data.car_user_relation.name}</a> - ${data.car_user_relation.email}</strong>`);
                })
            });

            // SHOW --------------------------------
            // UPDATE ------------------------------

            $('body').on('click', '.editCar', function () {
                var car_id = $(this).data('id');
                $.get("{{ route('cars.index') }}" + '/' + car_id + '/edit', function (data) {
                    $('#edit-modelHeading').html("Editar carro");
                    $('#saveEditBtn').val("Editar carro");
                    $('#saveEditBtn').attr("disabled", false);
                    $('#saveEditBtn').attr('data-id', data.car.id);
                    $('#editCar').modal('show');
                    // $('#edit_user_id').val(data.id);                                                            
                    $('#edit_car_brand').val(data.car.car_brand);
                    $('#edit_car_model').val(data.car.car_model);
                    $('#edit_car_plate').val(data.car.car_plate);
                    $('#edit_car_seats').val(data.car.car_seats);
                   if (data.users.length === 0 && data.relation.lenght === null) {
                       document.getElementById('edit_user_id').innerHTML = `<option value="">No hay usuarios</option>`;                    
                    } else {
                        let template = `<option value="">Asigne un usuario para que conduzca este carro</option>`;
                        template += `<option value="${data.relation.id}" selected>${data.relation.name}</option>`;
                        $.each(data.users, function (i, value) {                                                               
                             template += `<option value="${value.id}">${value.name}</option>`;                            
                        })
                            document.getElementById('edit_user_id').innerHTML = template;
                    }                                        
                })
            });

            $('#saveEditBtn').click(function (e) {
                e.preventDefault();
                $(this).val('Enviando..');
                $('#saveEditBtn').attr("disabled", true);
            
                var car_id = $(this).data('id');
                $.ajax({
                    data: $('#carEditForm').serialize(),
                    url: "{{ route('cars.index') }}" + '/' + car_id,
                    type: "PUT",
                    dataType: 'json',
                    success: function (data) {
                        $('#editCar').modal('hide');
                        Toast.fire({
                            type: 'success',
                            title: `${data.success}`
			            });                                                
                        table.draw();
                    },
                    error: function (data) {                        
                        $('#saveEditBtn').val("Editar usuario");
                        $('#saveEditBtn').attr('disabled', false);
                        $.each(data.responseJSON.errors, function (i, value) {                            
                            let editPrefix = 'edit_'+i;                                                                                      
                            if (editPrefix === document.getElementById(editPrefix).id) {                                                                
                                $(`#${editPrefix}`).addClass('is-invalid');                                
                                $.each(value, function (j, value) {                                                                        
                                    let editPrefix2 = 'error_' + editPrefix;
                                    document.getElementById(editPrefix2).innerHTML = `<strong>${value}</strong>`;
                                })
                            } else {
                                $(`#${editPrefix}`).removeClass('is-invalid');
                            }
                        });
                    }
                });
            });

            // UPDATE ------------------------------
            // DELETE ------------------------------

            $('body').on('click', '.deleteCar', function () {
                var car_id = $(this).data("id");                           
                Swal.fire({
                    title: '¿Estas seguro de eliminarlo?',
                    text: "Recuerda que este carro esta asociado a un usuario.",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Si, eliminar'
                }).then((result) => {
                    if (result.value) {
                        jQuery.ajax({
                            url: '{{ url('admin/cars') }}' + '/' + car_id,
                            method: 'DELETE',
                            data: {
                                id: car_id
                            },
                            success: function (result) {
                                if (result.error) {
                                    Swal.fire(
                                        'Error!',
                                        `${result.error}`,
                                        'error'
                                    )
                                } else {
                                    Swal.fire(
                                        'Eliminado!',
                                        `${result.success}`,
                                        'success'
                                    )
                                    table.draw();
                                }
                            },
                            error: function (data) {
                                console.log(data);
                            }
                        });
                    }
                });
            });

            // DELETE ------------------------------

        });
    </script>

@endpush