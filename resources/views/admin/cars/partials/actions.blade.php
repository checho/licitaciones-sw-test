<style>
    .my-button-info {
        margin: 5px;
        padding: 5px;
        /* height: 20px; */
        /* width: 20%; */
        border: solid 1px black;
    }
    a.my-button-info:hover {
        background: #17a2b8;
        color: #ffffff;
        text-decoration: none;
    }

    .my-button-warning {
        margin: 5px;
        padding: 5px;
        /* height: 20px; */
        /* width: 20%; */
        border: solid 1px black;
    }
    a.my-button-warning:hover {
        background: #ffc107;
        color: #ffffff;
        text-decoration: none;
    }

    .my-button-danger {
        margin: 5px;
        padding: 5px;
        /* height: 20px; */
        /* width: 20%; */
        border: solid 1px black;
    }
    a.my-button-danger:hover {
        background: #dc3545;
        color: #ffffff;
        text-decoration: none;
    }
</style>

<div class="btn-group float-right">                                            
    <a class="my-button-info viewCar" href="javascript:void(0)" data-id="{{ $id }}"><i class="fas fa-align-left fa-eye"></i></a>
    <a class="my-button-warning editCar" href="javascript:void(0)" data-id="{{ $id }}"><i class="fas fa-user-edit"></i></a>
    <a class="my-button-danger deleteCar" href="javascript:void(0)" data-id="{{ $id }}"><i class="fa fa-trash"></i></a> 
</div>