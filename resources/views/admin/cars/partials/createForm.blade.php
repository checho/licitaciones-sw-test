@csrf

<div class="row">
    <div class="form-group col-md-6 {{ $errors->has('user_id') ? 'has-error' : '' }}">
        <label for="user_id">Asigne un usuario para que conduzca este carro</label>
        <select class="form-control select2-users " style="width: 100%;" name="user_id" required>
            <option value="" selected>Asigne un usuario para que conduzca este carrro</option>
            @forelse ($users as $user)
                <option value="{{ $user->id }}" {{ (collect(old('user_id'))->contains($user->id)) ? 'selected':'' }}>{{ $user->name }}</option>
            @empty
                <option value="">No hay usuarios</option>                
            @endforelse
        </select>
        @if ($errors->has('user_id'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('user_id') }}</strong>
            </span>
        @endif
    </div>
    <div class="col-md-6"></div>
    <div class="form-group col-md-6">
        <label for="car_brand">Marca del carro</label>
        <input class="form-control {{ $errors->has('car_brand') ? 'is-invalid' : '' }}" type="text" name="car_brand" value="{{ old('car_brand') }}" autofocus>
        @if ($errors->has('car_brand'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('car_brand') }}</strong>
            </span>
        @endif
    </div>
    <div class="form-group col-md-6">
        <label for="car_model">Modelo del carro</label>
        <input class="form-control {{ $errors->has('car_model') ? 'is-invalid' : '' }}" type="text" name="car_model" value="{{ old('car_model') }}">
        @if ($errors->has('car_model'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('car_model') }}</strong>
            </span>
        @endif
    </div>
    <div class="form-group col-md-6">
        <label for="car_plate">Placa del carro</label>
        <input class="form-control {{ $errors->has('car_plate') ? 'is-invalid' : '' }}" type="text" name="car_plate" value="{{ old('car_plate') }}">
        @if ($errors->has('car_plate'))
        <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('car_plate') }}</strong>
        </span>
        @endif
    </div>        
    <div class="form-group col-md-6">
        <label for="car_seats">Número de asientos del carro</label>
        <input class="form-control {{ $errors->has('car_seats') ? 'is-invalid' : '' }}" type="number" name="car_seats" value="{{ old('car_seats') }}" min="1" max="50">
        @if ($errors->has('car_seats'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('car_seats') }}</strong>
            </span>
        @endif
    </div>    

    <div class="col-md-12">
            <button type="submit" class="btn btn-outline-success float-left">Terminar <i class="fas fa-check"></i></button>
            <a href="{{ route('cars.index') }}" class="btn btn-outline-secondary float-right">Regresar <i class="fas fa-arrow-alt-circle-left"></i></a>                
    </div>
</div>


