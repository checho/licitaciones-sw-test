<div id="viewCar" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="view-modelHeading"></h5>
                
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">                                
                <div class="row">
                    <div class="col-md-6">
                        <h3>Marca del carro:</h3>
                        <div class="col-md-11 offset-1">
                            <strong id="view-car_brand"></strong>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <h3>Modelo del carro:</h3>
                        <div class="col-md-11 offset-1">
                            <strong id="view-car_model"></strong>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <h3>Placa del carro:</h3>
                        <div class="col-md-11 offset-1">
                            {{-- <strong id="member_since" >{{ $user->created_at->toFormattedDateString() }}</strong> --}}
                            <strong id="view-car_plate" ></strong>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <h3>Numero de asientos del carro:</h3>
                        <div class="col-md-11 offset-1">
                            {{-- <strong id="member_since" >{{ $user->created_at->toFormattedDateString() }}</strong> --}}
                            <strong id="view-car_seats" ></strong>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <h3>Usuario que maneja este carro:</h3>
                        <div id="car_user_relation" class="col-md-11 offset-1">
                                                        
                        </div>
                    </div>
                    <div class="col-md-12">
                        <hr>
                        <button type="button" class="btn btn-default waves-effect float-right" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>

            </div>            
        </div>
        <!-- /.modal-content -->
    </div>
</div>

@push('custom-scripts')    

   {{-- custom scripts --}}

@endpush