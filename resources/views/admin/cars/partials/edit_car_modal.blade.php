<div id="editCar" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="edit-modelHeading"></h5>
                
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="carEditForm" name="carEditForm">                                
                    <div class="row">
                        <div class="form-group col-md-12 ">
                            <label for="user_id">Asigne un usuario para que conduzca este carro</label>
                            <select id="edit_user_id" class="form-control select2-users " style="width: 100%;" name="user_id" autofocus required>
                                <option value="" selected>Asigne un usuario para que conduzca este carrro</option>
                            </select>                     
                                <span id="error_edit_user_id" class="invalid-feedback" role="alert">                                    
                                </span>                            
                        </div>
                        <div class="form-group col-md-6">
                            <label for="car_brand">Marca del carro</label>
                            <input id="edit_car_brand" class="form-control " type="text" name="car_brand" value="{{ old('car_brand') }}" required>                            
                                <span id="error_edit_car_brand" class="invalid-feedback" role="alert">                                    
                                </span>                            
                        </div>
                        <div class="form-group col-md-6">
                            <label for="car_model">Modelo del carro</label>
                            <input id="edit_car_model" class="form-control " type="number" name="car_model" value="{{ old('car_model') }}" min="1900" max="2020">                            
                                <span id="error_edit_car_model" class="invalid-feedback" role="alert">                                    
                                </span>                            
                        </div>
                        <div class="form-group col-md-6">
                            <label for="car_plate">Placa del carro</label>
                            <input id="edit_car_plate" class="form-control " type="text" name="car_plate" value="{{ old('car_plate') }}">                            
                                <span id="error_edit_car_plate" class="invalid-feedback" role="alert">                                
                                </span>                            
                        </div>        
                        <div class="form-group col-md-6">
                            <label for="car_seats">Número de asientos del carro</label>
                            <input id="edit_car_seats" class="form-control " type="number" name="car_seats" value="{{ old('car_seats') }}" min="1" max="50">                            
                                <span id="error_edit_car_seats" class="invalid-feedback" role="alert">                                    
                                </span>                            
                        </div>                        
                        <div class="col-md-12">                            
                            <input type="submit" class="btn btn-success" id="saveEditBtn" value="edit">
                            <button type="button" class="btn btn-default waves-effect float-right" data-dismiss="modal">Cancelar</button>
                        </div>      
                    </div>
                </form>
            </div>            
        </div>
        <!-- /.modal-content -->
    </div>
</div>

@push('custom-scripts')    

   {{-- custom scripts --}}

@endpush